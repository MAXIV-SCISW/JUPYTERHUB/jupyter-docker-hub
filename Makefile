###############################################################################
# 
# 	Jupyter Hub - Makefile
# 
#   This Makefile is used for building the jupyterhub image, pushing and 
#   pulling to and from the registry, as well as listing images that can be
#   built, exist on the loacal machine, and exist in the registry.
# 
#  	Run 'make help' for usage information
# 
###############################################################################

.DEFAULT_GOAL := help
SHELL := /bin/bash
.ONESHELL:
export PYTHONPATH=.

# Set some names 
IMAGE_NAMES=jupyterhub test-jupyterhub
REG=docker.maxiv.lu.se

# Colors
LIGHTPURPLE := \033[1;35m
GREEN := \033[32m
CYAN := \033[36m
BLUE := \033[34m
RED := \033[31m
NC := \033[0m

# Seperator lines
line_len=80
print_line_green = printf "$(GREEN)"; printf "%0.s*" {1..$(line_len)}; \
				   printf "$(NC)\n"  
print_line_blue = printf "$(BLUE)"; printf "%0.s*" {1..$(line_len)}; \
				  printf "$(NC)\n"  
print_line_red = printf "$(RED)"; printf "%0.s*" {1..$(line_len)}; \
				 printf "$(NC)\n"  

THIS_FILE := $(lastword $(MAKEFILE_LIST))

###############################################################################
##@ Help
###############################################################################

.PHONY: help

help:  ## Display this help message
	@printf "\n"
	$(print_line_blue)
	printf "\n$(RED)Jupyter Hub $(BLUE)Makefile$(NC)\n"
	printf "    This Makefile is used for building the jupyterhub image,\n" 
	printf "    pushing and pulling to and from the registry, as well as\n"
	printf "    listing images that can be built, exist on the loacal\n"
	printf "    machine, and exist in the registry $(REG)\n"
	printf "\n$(BLUE)Usage\n    $(CYAN)make $(NC)<target>\n"
	printf "\n$(BLUE)Examples$(NC)\n"
	printf "    $(CYAN)make $(NC)list-build$(NC)\n"
	printf "    $(CYAN)make $(NC)list-local$(NC)\n"
	printf "    $(CYAN)make $(NC)list-registry$(NC)\n"
	printf "    $(CYAN)make $(NC)build/jupyterhub$(NC)\n"
	printf "    $(CYAN)make $(NC)tag-registry/jupyterhub$(NC)\n"
	printf "    $(CYAN)make $(NC)push/jupyterhub$(NC)\n"
	printf "    $(CYAN)make $(NC)clean/jupyterhub$(NC)\n"
	printf "    $(CYAN)make $(NC)pull/jupyterhub$(NC)\n"
	awk 'BEGIN {FS = ":.*##";} /^[a-zA-Z_-].*##/ \
	{ printf "    $(CYAN)%-15s$(NC) %s\n", $$1, $$2} /^##@/ \
	{ printf "\n$(BLUE)%s$(NC)\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
	$(print_line_blue)


# This command is to sorta check that a python virtualenv exists, and if not, 
# create it
.PHONY: venv venv-clean
venv:
	@if [ ! -d .env ] ; 
	then 
	 	echo "virtualenv does not exist, creating..."; 
	 	virtualenv -p python3 .env;
		source .env/bin/activate;
		pip3 install -Uq pip;
		pip3 install -r helpers/make-requirements.txt; 
	fi

venv-clean:
	rm -rf .env
 

###############################################################################
##@ Image Lists
###############################################################################

.PHONY: list-build list-local list-registry 

list-build: ## List names of all notebook images that can be built
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)Hub Images:$(CYAN)"
	echo -e $(foreach i,$(IMAGE_NAMES),"\n    $(i)")
	printf "$(NC)"
	$(print_line_blue)

list-local: venv ## List all notebook images present on the local host
	source .env/bin/activate;
	@python3 helpers/list_local_notebook_images.py "$(IMAGE_NAMES)"

list-registry: venv ## List all relevant images and tags in the registry 
	source .env/bin/activate;
	@python3 helpers/list_registry_notebook_images.py "$(IMAGE_NAMES)" 


###############################################################################
##@ Build Images
###############################################################################

.PHONY: build 

build/%: venv ## Builds specific notebook image with latest tag. Replace '%' with notebook name
	source .env/bin/activate;
	python3 helpers/build_notebook_images.py "$(notdir $@)"


###############################################################################
##@ Push Images
###############################################################################

.PHONY: push 

push/%: venv ## Push a single image with the current tag value to the docker registry
	source .env/bin/activate;
	python3 helpers/push_notebook_images.py "$(notdir $@)"


###############################################################################
##@ Pull Images
###############################################################################

.PHONY: pull 

pull/%: venv ## Pull a single image with the current tag value from the docker registry
	source .env/bin/activate;
	python3 helpers/pull_notebook_images.py "$(notdir $@)"


###############################################################################
##@ Tag Images
###############################################################################

.PHONY: tag-local tag-stable tag-registry 

tag-stable/%: venv ## Tag with suffix of stable. Replace '%' with notebook name
	source .env/bin/activate;
	python3 helpers/tag_notebook_images.py --stable "$(notdir $@)"

tag-local/%: venv ## Tag with suffix of local. Replace '%' with notebook name
	source .env/bin/activate;
	python3 helpers/tag_notebook_images.py --local "$(notdir $@)"

tag-registry/%: venv ## Tag with prefix of registry name. Replace '%' with notebook name
	source .env/bin/activate;
	python3 helpers/tag_notebook_images.py --registry_tag --registry_stable "$(notdir $@)"


###############################################################################
##@ Clean Images
###############################################################################

.PHONY: clean-old clean-dangling remove

clean-old/%: venv ## Remove dangling and old images of a notebook.
	source .env/bin/activate;
	python3 helpers/remove_images.py --old "$(notdir $@)"

clean-dangling/%: venv ## Remove dangling and old images of a notebook.
	source .env/bin/activate;
	python3 helpers/remove_images.py --dangling "$(notdir $@)"

remove/%: venv ## Remove all instances of this notebook image. 
	source .env/bin/activate;
	python3 helpers/remove_images.py --dangling --all "$(notdir $@)"

remove-all: venv ## Remove all instances of this notebook image. 
	source .env/bin/activate;
	python3 helpers/remove_images.py --dangling --all "$(IMAGE_NAMES)"
