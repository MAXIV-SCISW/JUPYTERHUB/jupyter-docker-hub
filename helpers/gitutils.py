"""
Utilities for calling out to git
"""
import subprocess


def first_alpha(s):
    """
    Returns the length of the shortest substring of the input that
    contains an alpha character.
    """
    for i, c in enumerate(s):
        if c.isalpha():
            return i + 1
    raise Exception("No alpha characters in string: {}".format(s))


def substring_with_alpha(s, min_len=7):
    """
    Returns the shortest substring of the input that
    contains an alpha character.

    Used to avoid helm/go bug that converts a string with all digit
    characters into an exponential.
    """
    return s[:max(min_len, first_alpha(s))]


def last_modified_commit(paths, **kwargs):
    """Get the last commit which modified this image
       Usually this is just the Dockerfile, but it could also be configuration
       files and images on which this image is based upon.
    """
    args = [
        'git',
        'log',
        '-n', '1',
        '--pretty=format:%H',
        '--'
    ]
    for file_path in paths:
        args.append(file_path)

    commit_hash = subprocess.check_output(
        args, **kwargs).decode('utf-8').split('\n')[-1]

    return substring_with_alpha(commit_hash)


def last_modified_date(paths, **kwargs):
    """Return the last modified date (as a string) for the given paths"""
    args = [
        'git',
        'log',
        '-n', '1',
        '--pretty=format:%cd',
        '--date=short',
        '--'
    ]
    for file_path in paths:
        args.append(file_path)

    return subprocess.check_output(args, **kwargs).decode('utf-8')


def last_modified_unix_timestamp(paths, **kwargs):
    """Return the last modified date (as a unix timestamp) for the given paths
    """
    args = [
        'git',
        'log',
        '-n', '1',
        '--pretty=format:%ct',
        '--'
    ]
    for file_path in paths:
        args.append(file_path)

    return subprocess.check_output(args, **kwargs).decode('utf-8')


def path_touched(paths, commit_range):
    """Return whether the given paths have been changed in the commit range

    Used to determine if a build is necessary

    Args:
    *paths (str):
        paths to check for changes
    commit_range (str):
        range of commits to check if paths have changed
    """
    return subprocess.check_output([
        'git', 'diff', '--name-only', commit_range, '--', paths
    ]).decode('utf-8').strip() != ''
