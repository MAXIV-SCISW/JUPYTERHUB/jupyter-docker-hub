# JUPYTER HUB

## OVERVIEW

This is a jupyterhub docker image that uses the following extensions:
* CAS authentication
* Swarm Spawner

## GIT CODE
```bash
git clone git@gitlab.maxiv.lu.se:scisw/jupyterhub/jupyter-docker-hub.git
```

## BUILD IMAGE
```bash
cd jupyter-docker-hub/
docker build -t maxiv/jupyterhub:latest --no-cache .
```
