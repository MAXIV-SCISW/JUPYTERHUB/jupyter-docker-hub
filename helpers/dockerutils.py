"""
Utilities for calling out to docker
"""
import re
import datetime
import json
import subprocess
import os
from ruamel.yaml import YAML
yaml = YAML(typ='safe')

# Make it purrty
LIGHTPURPLE = "\033[1;35m"
GREEN = "\033[0;32m"
BLUE = "\033[34m"
CYAN = "\033[1;36m"
RED = "\033[31m"
NC = "\033[0m"


def execute_command_print_output(args):
    """Execute a docker command in a subprocess, printing the output in real
       time - as best as possible anyway.
    """

    # Print what is to be executed, use some color
    print('')
    print(GREEN, '*'.center(80, '*'), NC)
    print(GREEN, '**', BLUE, 'executing command:   ', LIGHTPURPLE, end="")
    for arg in args:
        print(arg, '', end='')
    print(NC)

    # Start the subprocess
    my_env = os.environ.copy()
    process = subprocess.Popen(args,
                               shell=False, bufsize=-1,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               env=my_env)

    # Send the output to the terminal in ~ real time
    for line in iter(process.stdout.readline, b''):
        print('  ', line.decode('utf-8'), end='')
    process.stdout.close()
    process.wait()

    # Print that it is done
    print(GREEN, '**', BLUE, 'Done executing command:   ', LIGHTPURPLE, end="")
    for arg in args:
        print(arg, '', end='')
    print(NC)
    print(GREEN, '*'.center(80, '*'), NC)


def execute_command_get_output(args):
    """Execute a docker command in a subprocess, suppress output to terminal
       and return the output in a variable.
    """

    output_value = False
    try:
        output_value = subprocess.check_output(args).decode('utf-8')
    except subprocess.CalledProcessError as e:
        print(e.output)

    return output_value


def docker_system_prune():
    """Clean up a bit, print output to the terminal."""
    execute_command_print_output(['docker', 'system', 'prune', '-f'])


def docker_push_image(full_image_name_with_tag):
    """Push an image. Print out to terminal."""
    execute_command_print_output([
        'docker', 'push',
        full_image_name_with_tag,
    ])


def docker_pull_image(full_image_name_with_tag):
    """Push an image. Print out to terminal."""
    execute_command_print_output([
        'docker', 'pull',
        full_image_name_with_tag,
    ])


def docker_tag_image(full_image_name_with_tag, new_image_name_with_tag):
    """Tag an image. Suppress output"""
    execute_command_get_output([
        'docker', 'tag',
        full_image_name_with_tag,
        new_image_name_with_tag
    ])


def docker_image_rm(full_image_name_with_tag):
    """Remove an image with a specific tag. Suppress output"""
    execute_command_get_output([
        'docker', 'image', 'rm', '-f',
        full_image_name_with_tag
    ])


def does_image_exist_locally(full_image_name_with_tag):
    """Check if the image exists on the local host.
       The full image name plus tag needs to be given, e.g.:
           maxiv-scisw-jupyter/base-notebook-ubuntu:2019-10-23_28391b2
       Terminal output is suppressed, the return value will be empty if nothing
       is found.
    """

    does_image_exist = execute_command_get_output([
        'docker', 'image', 'ls',
        '--filter=reference=' + full_image_name_with_tag,
        '-q'
    ])

    return does_image_exist


def does_image_exist_registry(reg, proj, image_name, tag, debug=False):
    """Check if the image exists on the registry.
       Terminal output is suppressed, the return is False if nothing is found.
    """

    does_image_exist = False

    # Need to check if image tag exists. If not, a string will be returned with
    # the value of null
    registry_output = execute_command_get_output([
        'curl', '-sX', 'GET',
        'https://' + reg + '/v2/' + proj + '/' + image_name + '/manifests/' +
        tag
    ])

    if debug:
        print(registry_output)

    # Load the json output
    config = []
    if registry_output:
        config = yaml.load(registry_output)

    # Check if response is positive
    if 'errors' in config:
        if debug:
            print('shit')
    else:
        if 'tag' in config and 'name' in config:
            if debug:
                print('config:')
                print('   name:', config['name'])
                print('   tag: ', config['tag'])
            if config['tag'] == tag:
                does_image_exist = True

    return does_image_exist


def parse_image_ls_output(command_output, debug=False):

    # Output variables
    output_names = []
    output_tags = []
    output_ids = []

    if debug:
        print(command_output)

    if command_output:
        command_output = re.split("\n", command_output)
        if debug:
            print(command_output)
        for row in command_output:
            if row.startswith('REPOSITORY') or row == '':
                if debug:
                    print('skip')
            else:
                if debug:
                    print('row:', row)
                row = row.split()
                if debug:
                    print('row:', row)
                if len(row) > 2:
                    output_names.append(row[0])
                    output_tags.append(row[1])
                    output_ids.append(row[2])

    return output_names, output_tags, output_ids


def docker_get_image_list(proj_name, reg_proj_name, debug=False):
    """Get a list of dangling images for the given notebook
    """

    # Output variables
    do_images_exist = False
    output_names = []
    output_tags = []
    output_ids = []

    for image_name in [proj_name, reg_proj_name]:

        # Get a list of tags for this image
        args = ['docker', 'image', 'ls',
                '-f', 'reference=' + image_name]

        command_output = execute_command_get_output(args)

        names, tags, ids = parse_image_ls_output(command_output, False)

        output_names.extend(names)
        output_tags.extend(tags)
        output_ids.extend(ids)

    if debug:
        print(output_names)
        print(output_tags)
        print(output_ids)

    if len(output_names) > 0:
        do_images_exist = True

    return do_images_exist, output_names, output_tags, output_ids


def docker_get_image_list_dangling(proj_name, reg_proj_name, debug=False):
    """Get a list of danling images for the given notebook
    """

    # Output variables
    do_images_exist = False
    output_names = []
    output_tags = []
    output_ids = []

    for image_name in [proj_name, reg_proj_name]:

        # Get a list of tags for this image
        args = ['docker', 'image', 'ls',
                '-f', 'reference=' + image_name,
                '-f', 'dangling=true']

        command_output = execute_command_get_output(args)

        names, tags, ids = parse_image_ls_output(command_output, False)

        output_names.extend(names)
        output_tags.extend(tags)
        output_ids.extend(ids)

    if debug:
        print(output_names)
        print(output_tags)
        print(output_ids)

    if len(output_names) > 0:
        do_images_exist = True

    return do_images_exist, output_names, output_tags, output_ids


def docker_get_image_list_old(proj_name, reg_proj_name, tag, debug=False):
    """Get a list of old image tags, ignoring stable and local tags.
    """

    # Output variables
    do_images_exist = False
    output_names = []
    output_tags = []
    output_ids = []

    do_images_exist, image_names, image_tags, ids = \
        docker_get_image_list(proj_name, reg_proj_name, False)

    # Another way to filter
    for image_name, image_tag, image_id in \
            zip(image_names, image_tags, ids):

        # Look for any images that do not have the most recent, proper tag
        if image_tag != tag and image_tag != 'stable' and \
                image_tag != 'local':
            output_names.append(image_name)
            output_tags.append(image_tag)
            output_ids.append(image_id)

    if debug:
        print(output_names)
        print(output_tags)
        print(output_ids)

    if len(output_names) > 0:
        do_images_exist = True
    else:
        do_images_exist = False

    return do_images_exist, output_names, output_tags, output_ids


def registry_get_image_tags(reg, proj, image_name, debug=False):
    """Get information about an image from the registry.
    """

    # Output variables
    does_image_exist = False
    image_tags = []
    created_sorted = []
    ids = []

    # Get a list of tags for this iamge
    args = [
        'curl', '-sX', 'GET',
        'https://' + reg + '/v2/' + proj + '/' + image_name + '/tags/list'
    ]

    if debug:
        print(args)
        print(' '.join(args))

    registry_output = execute_command_get_output(args)

    if debug:
        print(registry_output)

    # Load the json output
    config = []
    if registry_output:
        config = yaml.load(registry_output)

    # Check if response is good
    if 'errors' in config:
        if debug:
            print('shit')
    else:
        if 'tags' in config and 'name' in config:
            if debug:
                print('config:')
                print('   name:', config['name'])
                print('   tags:', config['tags'])

            if config['tags'] is not None:
                does_image_exist = True
                image_tags = config['tags']
                created = []

                for tag in image_tags:
                    if debug:
                        print('   tag:          ', tag)
                    creation_date, layer_id = \
                        registry_get_image_creation_datestamp(
                            reg, proj, image_name, tag, False)
                    created.append(creation_date)
                    ids.append(layer_id)
                    if debug:
                        print('   creation_date:', creation_date)
                        print('   layer_id:     ', layer_id)
                        print('')

                # Sort the lists based on creation datetime stamp
                created, image_tags, ids = sort_three_lists(
                    created, image_tags, ids, False)

                # Convert datetime stamps to strings
                for creation_date in created:
                    created_sorted.append(
                        creation_date.strftime("%Y-%m-%d %H:%M:%S"))

    return does_image_exist, image_tags, created_sorted, ids


def registry_get_image_creation_datestamp(reg, proj, image_name, tag,
                                          debug=False):
    """Get the datetime stamp and registry id for a given image name and tag.
       Terminal output is suppressed, the return is nonsense if no image is
       found.
    """

    creation_date = datetime.datetime.min
    layer_id = '00000000000'

    # Need to check if image tag exists. If not, a string will be returned with
    # the value of null
    registry_output = execute_command_get_output([
        'curl', '-sX', 'GET',
        'https://' + reg + '/v2/' + proj + '/' + image_name + '/manifests/' +
        tag
    ])

    if debug:
        print(registry_output)

    # Load the json output
    config = []
    if registry_output:
        config = yaml.load(registry_output)

    # Check if response is good
    if 'errors' in config:
        if debug:
            print('shit')
    else:
        if 'history' in config:
            for element in config['history']:
                if 'v1Compatibility' in element:
                    v1Compatibility = element['v1Compatibility']

                    # This bit of the output seems to still be a string...
                    if isinstance(v1Compatibility, str):
                        v1Compatibility = json.loads(v1Compatibility)

                    # Get the creation datetime stamp
                    if 'created' in v1Compatibility:
                        timestamp_str = v1Compatibility['created']

                        # Convert the string into a datetime object
                        timestamp_str = timestamp_str.replace('T', ' ')
                        timestamp_str = timestamp_str.replace('Z', '')
                        timestamp_str = timestamp_str.split('.', 1)[0]
                        timestamp_obj = datetime.datetime.strptime(
                            timestamp_str, '%Y-%m-%d %H:%M:%S')

                        if debug:
                            print(timestamp_obj)

                        # Find the most recent datetime stamp from among all
                        # the layers
                        if timestamp_obj > creation_date:
                            creation_date = timestamp_obj
                            # Keep part of the layer id.
                            # NOTE: This is not the same as the image id that
                            #       is listed by 'docker image ls'
                            layer_id = v1Compatibility['id'][0:11]

    return creation_date, layer_id


def sort_three_lists(list_to_sort_on, list1, list2, debug=False):
    """Sort three lists based on the values in the first list"""

    zipped = list(zip(list_to_sort_on, list1, list2))
    zipped.sort(reverse=True)

    if debug:
        for row in zipped:
            print('   * row:', row)
            for col in row:
                print('     col:', col)

    return zip(*zipped)
