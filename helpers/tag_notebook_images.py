"""
This script looks for images with the most up to date and proper tag, then
tags it appropriately:
  Possible suffixes:
    :local      - for local development work
    :testing    - ready to send off for testing (process yet to be implemented)
    :stable     - for use on the production JupyterHub server
  Possible prefixes:
    docker.maxiv.lu.se/     - needed for sending to the registry
"""

#####################
# IMPORT LIBRARIES ##
#####################

# General systems
import sys
import argparse

from helpers import configure_notebook_images as cni
from helpers import list_local_notebook_images as llni
from helpers import dockerutils

# Make it purrty
LIGHTPURPLE = "\033[1;35m"
GREEN = "\033[0;32m"
BLUE = "\033[34m"
CYAN = "\033[1;36m"
RED = "\033[31m"
NC = "\033[0m"


###########
# HELPERS #
###########

def tag_notebook_images(nb_image_setup, action, count=0):

    # Loop over two image names to look for:
    #   1) the image name plus current tag
    #   2) the image name plus current tag, prepended with registry name
    image_exists_locally = False
    image_has_been_tagged = False
    for tagged_image_name in [nb_image_setup.proj_name_tag,
                              nb_image_setup.reg_proj_name_tag]:
        # print('tagged_image_name:   ', tagged_image_name)

        tagged_image_exists = dockerutils.does_image_exist_locally(
            tagged_image_name)

        # print('tagged_image_exists:   ', tagged_image_exists)

        if tagged_image_exists and not image_has_been_tagged:

            image_exists_locally = True

            new_tagged_image_name = False
            if action == 'stable':
                new_tagged_image_name = nb_image_setup.proj_name_stable
            if action == 'local':
                new_tagged_image_name = nb_image_setup.proj_name_local
            if action == 'registry_tag':
                new_tagged_image_name = nb_image_setup.reg_proj_name_tag
            if action == 'registry_stable':
                new_tagged_image_name = nb_image_setup.reg_proj_name_stable

            if new_tagged_image_name != '' and new_tagged_image_name:

                # Print a bit to the terminal about what will happen
                print(GREEN, '*'.center(102, '*'), NC)
                print(GREEN, '**', BLUE,
                      'Will now create a new tagged version of', CYAN,
                      nb_image_setup.name, NC)
                print(GREEN, '**  ', CYAN, new_tagged_image_name, NC)

                # Perform the tagging
                dockerutils.docker_tag_image(tagged_image_name,
                                             new_tagged_image_name)
                image_has_been_tagged = True
                print(GREEN, '*'.center(102, '*'), NC)

    # Complain
    if not image_exists_locally:
        print('')
        print(RED, '*'.center(102, '*'), NC)
        print(RED, '**', BLUE, 'The image', LIGHTPURPLE,
              nb_image_setup.name, NC, BLUE, 'has not been built with the',
              'proper tag of', NC, LIGHTPURPLE, nb_image_setup.tag, NC)
        print(RED, '**', BLUE, 'Build it first with: ', CYAN,
              'make build/' + nb_image_setup.name, NC)
        print(RED, '*'.center(102, '*'), NC)
        print('')


########
# MAIN #
########

def main(argv):

    '''
    The main function - usage and help, argument parsing
    '''

    # Setup options
    parser = argparse.ArgumentParser(
        description='Reduce (downsample) an image contained in an hdf5 file')
    parser.add_argument("input_string", nargs='*',
                        help='The input hdf5 file name')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='debug output')
    parser.add_argument('--local', action='store_true',
                        help='If the image exists with the current tag, create'
                             ' a new tag of that image of \'local\'')
    parser.add_argument('--stable', action='store_true',
                        help='If the image exists with the current tag, create'
                             ' a new tag of that image of \'stable\'')
    parser.add_argument('--registry_tag', action='store_true',
                        help='If the image exists with the current tag, create'
                             ' a new tag of that image prepended with the '
                             'registry name')
    parser.add_argument('--registry_stable', action='store_true',
                        help='If the image exists with the current tag, create'
                             ' a new tag of that image prepended with the '
                             'registry name and appended with \'stable\'')

    # Print a little extra in addition to the standard help message
    if len(argv) == 0 or '-h' in argv or '--help' in argv:
        try:
            args = parser.parse_args(['-h'])
        except SystemExit:
            print('')
            print('Examples of usage:')
            sys.exit()
    else:
        args = parser.parse_args(argv)

    if args.debug:
        print(args)

    # Parse the image names - could be a long string of names, or a list
    image_names = []
    for element in args.input_string:
        image_names.extend(args.input_string[0].split())

    # Perform one or more actions
    actions = []
    if args.local:
        actions.append('local')
    if args.stable:
        actions.append('stable')
    if args.registry_tag:
        actions.append('registry_tag')
    if args.registry_stable:
        actions.append('registry_stable')

    # Exit if no known action was requested
    if len(actions) == 0:
        print('')
        print(RED, '*'.center(102, '*'), NC)
        print(RED, '**', BLUE,
              'No image type specified.  Feel sad and try again. ', NC)
        print(RED, '*'.center(102, '*'), NC)
        print('')
        sys.exit()

    print('')

    # Loop over each action
    for action in actions:

        # Loop over each image
        count = 0
        for image_name in image_names:

            image_name = image_name.strip()
            if args.debug:
                print('image_name:', image_name)

            # Get information about this notebook, taken from
            # notebooks-config.yml and the git commit history
            nb_image_setup = cni.notebook_setup(image_name)

            # Check if nb_image_setup returns something valid.
            if nb_image_setup.valid:

                # Tag the image
                tag_notebook_images(nb_image_setup, action, count)

                count = count + 1

            else:

                # Print an error message
                print(RED, '*'.center(102, '*'), NC)
                print(RED, '**', BLUE, 'The notebook name ', CYAN, image_name,
                      NC, BLUE, 'is not valid.', NC)
                print(RED, '*'.center(102, '*'), NC)

    print('')

    llni.list_multiple_images(image_names)


######################
# RUN THE APPLICATION #
#######################

if __name__ == '__main__':
    main(sys.argv[1:])
