"""
This script looks for images with the most up to date and proper tag,
in the registry, and then pulls that image.
Then a local tag of the image is made, which has the registry address removed
and 'stable' as the suffix tag.
This is done in order to avoid JupyterHub from trying to pull an image when a
new session is requested by a user.
"""

import sys
from helpers import dockerutils
from helpers import configure_notebook_images as cni

# Make it purrty
LIGHTPURPLE = "\033[1;35m"
GREEN = "\033[0;32m"
BLUE = "\033[34m"
CYAN = "\033[1;36m"
RED = "\033[31m"
NC = "\033[0m"


def docker_list_local_image(nb_image_setup, count=0):

    # This returns True if the image exists locally
    does_image_exist, image_names, image_tags, ids = \
        dockerutils.docker_get_image_list(
            nb_image_setup.proj_name, nb_image_setup.reg_proj_name, False)

    # If the image exists, print information about it
    if does_image_exist:
        if count == 0:
            print('')
            print(GREEN, '*'.center(80, '*'), NC)
        print(GREEN, '**', BLUE, 'The image', CYAN, nb_image_setup.name, NC,
              BLUE, 'exists locally:', NC)

        col_width = 21
        count_reg = 0
        count_non_reg = 0
        for image_name, image_tag, image_id in \
                zip(image_names, image_tags, ids):

            if image_name.startswith(nb_image_setup.reg) and count_reg == 0:
                print(GREEN, '**  ', CYAN, nb_image_setup.reg_proj_name, NC)
                print(GREEN, '**', BLUE,
                      '      image tag              registry id', NC)
                count_reg += 1
            if not image_name.startswith(nb_image_setup.reg) and \
                    count_non_reg == 0:
                print(GREEN, '**  ', CYAN, nb_image_setup.proj_name, NC)
                print(GREEN, '**', BLUE,
                      '      image tag              registry id', NC)
                count_non_reg += 1

            if image_tag == nb_image_setup.tag:
                print(GREEN, '**     ', CYAN,
                      GREEN, image_tag.ljust(col_width), CYAN, image_id, NC)
            else:
                print(GREEN, '**     ', CYAN,
                      CYAN, image_tag.ljust(col_width), CYAN, image_id, NC)

        print(GREEN, '*'.center(80, '*'), NC)

    # If the image does not exist, complain
    else:
        print(RED, '*'.center(80, '*'), NC)
        print(RED, '**', BLUE, 'The image does not exist locally:', NC,
              CYAN, nb_image_setup.name, NC)
        print(RED, '*'.center(80, '*'), NC)


def list_multiple_images(image_names):
    count = 0
    for image_name in image_names:
        image_name = image_name.strip()
        # print("image_name:", image_name)

        # Get information about this image, taken from notebooks-config.yml and
        # the git commit history
        nb_image_setup = cni.notebook_setup(image_name)

        # Check if nb_image_setup returns something valid.
        if nb_image_setup.valid:

            # Print information about this image on the local machine
            docker_list_local_image(nb_image_setup, count)
            count = count + 1

        else:

            # Print an error message
            print(RED, '*'.center(80, '*'), NC)
            print(RED, '**', BLUE, 'The notebook name ', CYAN, image_name, NC,
                  BLUE, 'is not valid.', NC)
            print(RED, '*'.center(80, '*'), NC)


########
# MAIN #
########

def main(argv):

    # print(argv)

    # Input checks here (should add more)
    if len(argv) == 0:
        print(RED, '**', BLUE, "Bad input, try again!", RED, '**', NC)
        return
    # print(argv[0])

    image_names = argv[0].split()
    # print(image_names)

    # Loop over all the images names, print information on each
    list_multiple_images(image_names)


#######################
# RUN THE APPLICATION #
#######################

if __name__ == '__main__':
    main(sys.argv[1:])
