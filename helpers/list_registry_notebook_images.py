"""
This script looks for images with the most up to date and proper tag,
in the registry, and then pulls that image.
Then a local tag of the image is made, which has the registry address removed
and 'stable' as the suffix tag.
This is done in order to avoid JupyterHub from trying to pull an image when a
new session is requested by a user.
"""

import sys
from helpers import dockerutils
from helpers import configure_notebook_images as cni

# Make it purrty
LIGHTPURPLE = "\033[1;35m"
GREEN = "\033[0;32m"
BLUE = "\033[34m"
CYAN = "\033[1;36m"
RED = "\033[31m"
NC = "\033[0m"


def docker_list_registry_image(nb_image_setup, count=0):

    # This returns True if the image exists in the registry.
    image_exists_registry, image_tags, created, ids = \
        dockerutils.registry_get_image_tags(
            nb_image_setup.reg, nb_image_setup.proj, nb_image_setup.name,
            False)

    # If the image exists, print information about it
    if image_exists_registry:
        if count == 0:
            print('')
            print(GREEN, '*'.center(80, '*'), NC)
        print(GREEN, '**', BLUE, 'The image', CYAN, nb_image_setup.name, NC,
              BLUE, 'exists in the registry:', NC)
        print(GREEN, '**  ', CYAN, nb_image_setup.reg_proj_name, NC)
        print(GREEN, '**', BLUE,
              '      image tag              build date time       registry id',
              NC)
        col_width = 21
        for image_tag, image_date, image_id in zip(image_tags, created, ids):
            if image_tag == nb_image_setup.tag:
                print(GREEN, '**     ', CYAN, GREEN,
                      image_tag.ljust(col_width), CYAN,
                      image_date.ljust(col_width), image_id, NC)
            else:
                print(GREEN, '**     ', CYAN, CYAN,
                      image_tag.ljust(col_width), CYAN,
                      image_date.ljust(col_width), image_id, NC)

        print(GREEN, '*'.center(80, '*'), NC)

    # If the image does not exist, complain
    else:
        print(RED, '*'.center(80, '*'), NC)
        print(RED, '**', BLUE, 'The image is not in the registry:', NC)
        print(RED, '**  ', CYAN, nb_image_setup.reg_proj_name, NC)
        print(RED, '*'.center(80, '*'), NC)


########
# MAIN #
########

def main(argv):

    # print(argv)

    # Input checks here (should add more)
    if len(argv) == 0:
        print(RED, '**', BLUE, "Bad input, try again!", RED, '**', NC)
        return
    # print(argv[0])

    image_names = argv[0].split()
    # print(image_names)

    count = 0
    for image_name in image_names:
        image_name = image_name.strip()
        # print("image_name:", image_name)

        # Get information about this image, taken from notebooks-config.yml and
        # the git commit history
        nb_image_setup = cni.notebook_setup(image_name)

        # Check if nb_image_setup returns something valid.
        if nb_image_setup.valid:

            # Print information about this image in the registry
            docker_list_registry_image(nb_image_setup, count)
            count = count + 1

        else:

            # Print an error message
            print(RED, '*'.center(80, '*'), NC)
            print(RED, '**', BLUE, 'The notebook name ', CYAN, image_name, NC,
                  BLUE, 'is not valid.', NC)
            print(RED, '*'.center(80, '*'), NC)


#######################
# RUN THE APPLICATION #
#######################

if __name__ == '__main__':
    main(sys.argv[1:])
