"""

"""


import os
import sys
from ruamel.yaml import YAML
from helpers import gitutils
yaml = YAML(typ='safe')


class LocalImage:
    """
    A docker image that can be built from a local filesystem source
    """

    def __init__(self, name, notebook_config_all, debug=True):
        """
        Create an Image
        name: Fully qualified name of image
        path: Absolute path to local directory with image contents
        """

        notebook_config = get_notebook_config_single(name,
                                                     notebook_config_all)
        if debug:
            print(notebook_config)

        if notebook_config:
            self.reg = notebook_config_all['REG']
            self.proj = notebook_config_all['PROJ']

            self.valid = True
            self.name = name
            self.path = os.path.join(notebook_config['dir'],
                                     notebook_config['os'])
            self.dir = notebook_config['dir']

            # Make a list of files in the 'dir' directory,
            # e.g. base-notebook/start.sh
            files_to_check = []
            dir_files = (dir_file for dir_file in os.listdir(self.dir)
                         if os.path.isfile(os.path.join(self.dir, dir_file)))
            for dir_file in dir_files:
                if debug:
                    print('dir_file:', dir_file)
                files_to_check.append(os.path.join(self.dir, dir_file))

            # Make a list of files in the 'path' directory,
            # e.g. base-notebook/ubuntu/Dockerfile
            dir_files = (dir_file for dir_file in os.listdir(self.path)
                         if os.path.isfile(os.path.join(self.path, dir_file)))
            for dir_file in dir_files:
                if debug:
                    print('dir_file:', dir_file)
                files_to_check.append(os.path.join(self.path, dir_file))

            for file_to_check in files_to_check:
                if debug:
                    print('file_to_check:', file_to_check)

            self.last_commit_date = gitutils.last_modified_date(files_to_check)
            self.last_commit_hash = gitutils.last_modified_commit(
                files_to_check)
            self.last_commit_ts = gitutils.last_modified_unix_timestamp(
                files_to_check)
            self.tag = self.last_commit_date + '_' + self.last_commit_hash

            self.display_name = notebook_config['display_name']
            self.image_from = notebook_config['image_from']

            # Now get the tag of the image the current image is built upon.
            # Done recursively.
            self.tag_from = 'undefined'
            self.last_commit_ts_from = 'undefined'
            nb_image_setup_from = 'undefined'

            if self.image_from is False or self.image_from == 'undefined':
                if debug:
                    print('** ', self.name,
                          'is not based upon a locally made image')
                    print('')
                self.tag_from = 'undefined'
                self.image_from = 'undefined'
                self.last_commit_ts_from = 'undefined'
            else:
                if debug:
                    print('** ', self.name, 'is based upon', self.image_from)
                    print('')
                nb_image_setup_from = LocalImage(self.image_from,
                                                 notebook_config_all,
                                                 debug)
                self.tag_from = nb_image_setup_from.tag
                self.last_commit_ts_from = nb_image_setup_from.last_commit_ts

            # The full image name as found in the registry for the base image
            self.full_reg_image_from = self.reg + '/' + self.proj + '/' + \
                self.image_from + ':' + self.tag_from

            # If the image that the current image is built upon is newer, use
            # that tag instead, Also use the newer last_commit_hash,
            # last_commit_date, and last_commit_ts.
            if self.last_commit_ts_from != 'undefined' and \
                    nb_image_setup_from != 'undefined':

                ts_from = int(self.last_commit_ts_from)
                ts = int(self.last_commit_ts)

                if debug:
                    print('  self.name:      ', self.name,       ' ts:', ts)
                    print('  self.image_from:', self.image_from, ' ts:',
                          ts_from)

                # Is the base image newer than the current image?
                if ts_from > ts:
                    if debug:
                        print('    --> Need to change tag of ', self.name)
                        print('    --> old tag:', self.tag)
                    self.tag = self.tag_from
                    self.last_commit_date = \
                        nb_image_setup_from.last_commit_date
                    self.last_commit_hash = \
                        nb_image_setup_from.last_commit_hash
                    self.last_commit_ts = nb_image_setup_from.last_commit_ts
                    if debug:
                        print('    --> new tag:', self.tag)
                else:
                    if debug:
                        print('    --> No need to change tag of ', self.name)
                if debug:
                    print('')

            # Some often used combinations of values:
            self.proj_name = self.proj + '/' + self.name
            self.proj_name_tag = self.proj_name + ':' + self.tag
            self.proj_name_stable = self.proj_name + ':stable'
            self.proj_name_local = self.proj_name + ':local'

            self.reg_proj_name = self.reg + '/' + self.proj_name
            self.reg_proj_name_tag = self.reg_proj_name + ':' + self.tag
            self.reg_proj_name_stable = self.reg_proj_name + ':stable'

        else:
            self.valid = False


def get_notebook_config_from_file(config_file):
    """
    Return configuration if it exists.
    """
    config_path = config_file

    if not os.path.exists(config_path):
        return {}

    with open(config_path) as f:
        config = yaml.load(f)
        # print(config)

    return config


def get_notebook_config_single(notebook_name, notebooks_config):

    image_config = {}

    if 'images' in notebooks_config:
        images_config = notebooks_config['images']
        # print(images_config)

        if notebook_name in images_config:
            image_config = images_config[notebook_name]
            # print(image_config)

    return image_config


def notebook_setup(nb_name, debug=False):

    notebooks_config = get_notebook_config_from_file('notebooks-config.yml')
    nb_image_setup = LocalImage(nb_name, notebooks_config, debug)

    return nb_image_setup


########
# MAIN #
########

def main(argv):

    nb_image_setup = notebook_setup(argv[0], True)

    if nb_image_setup.valid:
        print('nb_image_setup.name:                 ', nb_image_setup.name)
        print('nb_image_setup.path:                 ', nb_image_setup.path)
        print('nb_image_setup.last_commit_hash:     ',
              nb_image_setup.last_commit_hash)
        print('nb_image_setup.last_commit_date:     ',
              nb_image_setup.last_commit_date)
        print('nb_image_setup.ts:                   ',
              nb_image_setup.last_commit_ts)
        print('nb_image_setup.tag:                  ', nb_image_setup.tag)
        print('nb_image_setup.reg:                  ', nb_image_setup.reg)
        print('nb_image_setup.proj:                 ', nb_image_setup.proj)
        print('nb_image_setup.image_from:           ',
              nb_image_setup.image_from)
        print('nb_image_setup.tag_from:             ', nb_image_setup.tag_from)

        print('nb_image_setup.proj_name:            ',
              nb_image_setup.proj_name)
        print('nb_image_setup.proj_name_tag:        ',
              nb_image_setup.proj_name_tag)
        print('nb_image_setup.proj_name_stable:     ',
              nb_image_setup.proj_name_stable)
        print('nb_image_setup.proj_name_local:      ',
              nb_image_setup.proj_name_local)

        print('nb_image_setup.reg_proj_name:        ',
              nb_image_setup.reg_proj_name)
        print('nb_image_setup.reg_proj_name_tag:    ',
              nb_image_setup.reg_proj_name_tag)
        print('nb_image_setup.reg_proj_name_stable: ',
              nb_image_setup.reg_proj_name_stable)

        print('nb_image_setup.full_reg_image_from:  ',
              nb_image_setup.full_reg_image_from)
    else:
        print('The notebook name ', argv[0], 'is not valid.')


#######################
# RUN THE APPLICATION #
#######################

if __name__ == '__main__':
    main(sys.argv[1:])
