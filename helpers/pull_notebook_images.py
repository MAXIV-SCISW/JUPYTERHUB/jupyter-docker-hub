"""
This script looks for images with the most up to date and proper tag,
in the registry, and then pulls that image.
Then a local tag of the image is made, which has the registry address removed
and 'stable' as the suffix tag.
This is done in order to avoid JupyterHub from trying to pull an image when a
new session is requested by a user.
"""

import sys
from helpers import dockerutils
from helpers import configure_notebook_images as cni
from helpers import list_local_notebook_images as llni

# Make it purrty
LIGHTPURPLE = "\033[1;35m"
GREEN = "\033[0;32m"
BLUE = "\033[34m"
CYAN = "\033[1;36m"
RED = "\033[31m"
NC = "\033[0m"


def docker_pull_and_tag(nb_image_setup):

    # This functions will:
    #   - pull the an image from the registry with the most recent tag

    # All the parts of the image name:
    registry = nb_image_setup.reg
    project = nb_image_setup.proj
    name = nb_image_setup.name
    tag = nb_image_setup.tag

    # Assemble the expected tagged image name, including registry prefix and
    # suffix:
    registry_tagged_image_name = nb_image_setup.reg_proj_name_tag

    # This returns True if the image exists in the registry.
    image_exists_registry = dockerutils.does_image_exist_registry(
        registry, project, name, tag)

    # If the image exists, pull it
    if image_exists_registry:
        print('')
        print(GREEN, '*'.center(80, '*'), NC)
        print(GREEN, '**', BLUE,
              'The image', CYAN, name, NC, BLUE,
              'with the tag of', CYAN, tag, NC, BLUE,
              'exists in the registry:', NC)
        print(GREEN, '**  ', CYAN, registry_tagged_image_name, NC)

        # Print a bit to the terminal about what will happen
        print(GREEN, '**', BLUE, 'Will now pull this image to local machine',
              NC)
        print(GREEN, '*'.center(80, '*'), NC)
        print('')

        # Perform the pulling
        dockerutils.docker_pull_image(registry_tagged_image_name)

        # Check that the pulled image exists on the local machine
        pulled_image_exists = dockerutils.does_image_exist_locally(
            registry_tagged_image_name)

        # Print error if image not pulled
        if not pulled_image_exists:
            print('')
            print(RED, '*'.center(80, '*'), NC)
            print(RED, '**', BLUE, 'Something went wrong with pulling:', NC)
            print(RED, '**  ', CYAN, registry_tagged_image_name, NC)
            print(RED, '*'.center(80, '*'), NC)
            print('')

    # If the image does not exist in the registry, complain
    else:
        print('')
        print(RED, '*'.center(80, '*'), NC)
        print(RED, '**', BLUE, 'Image is not in the registry:', NC)
        print(RED, '**  ', CYAN, registry_tagged_image_name, NC)
        print(RED, '*'.center(80, '*'), NC)
        print('')


########
# MAIN #
########

def main(argv):

    # print(argv)

    image_name = False

    # Input checks here (should add more)
    if len(argv) > 0:
        image_name = argv[0]
    else:
        print("Bad input, try again!")
        return

    # Get information about this image, taken from notebooks-config.yml and the
    # git commit history
    nb_image_setup = cni.notebook_setup(image_name)

    # Check if nb_image_setup returns something valid.
    if nb_image_setup.valid:

        # Pull the proper image from the registry
        docker_pull_and_tag(nb_image_setup)

    else:
        print('The notebook name ', image_name, 'is not valid.')

    # Print information about the images that were pulled.
    llni.list_multiple_images([image_name])


#######################
# RUN THE APPLICATION #
#######################

if __name__ == '__main__':
    main(sys.argv[1:])
