#!/usr/bin/env python
"""
A python version of the build script.
Kind of a test to see if I like using python for this instead of bash - still
not sure...
"""

#####################
# IMPORT LIBRARIES ##
#####################

# General systems
import sys
import argparse

# Local resources
from helpers import configure_notebook_images as cni
from helpers import list_local_notebook_images as llni
from helpers import dockerutils

# Make it purrty
LIGHTPURPLE = "\033[1;35m"
GREEN = "\033[0;32m"
BLUE = "\033[34m"
CYAN = "\033[1;36m"
RED = "\033[31m"
NC = "\033[0m"


###########
# HELPERS #
###########

def docker_list_and_delete_images(nb_image_setup, action):

    # This will True if any images exists in the registry.
    do_images_exist = False
    if action == 'dangling':
        do_images_exist, image_names, image_tags, ids = \
            dockerutils.docker_get_image_list_dangling(
                nb_image_setup.proj_name, nb_image_setup.reg_proj_name, False)
    if action == 'old':
        do_images_exist, image_names, image_tags, ids = \
            dockerutils.docker_get_image_list_old(
                nb_image_setup.proj_name, nb_image_setup.reg_proj_name,
                nb_image_setup.tag, False)
    if action == 'all':
        do_images_exist, image_names, image_tags, ids = \
            dockerutils.docker_get_image_list(
                nb_image_setup.proj_name, nb_image_setup.reg_proj_name, False)

    # If images exists, print some information
    i_did_something = False
    if do_images_exist:
        print('')
        print(BLUE, '*'.center(80, '*'), NC)
        print(BLUE, '**', BLUE, 'Deleting instances of', LIGHTPURPLE,
              nb_image_setup.name, NC, end="")

        if action == 'old':
            print(BLUE, 'that do not have the tag', GREEN, nb_image_setup.tag)
        if action == 'dangling':
            print(BLUE, 'that are dangling:')
        if action == 'all':
            print('')

        col_width0 = 64
        col_width1 = 18

        print(BLUE, '**', BLUE, str('image name').ljust(col_width0),
              BLUE, str('tag').ljust(col_width1), BLUE, str('image id'), NC)

        # Loop over the images found
        for image_name, image_tag, image_id in \
                zip(image_names, image_tags, ids):

            print(BLUE, '**', CYAN, image_name.ljust(col_width0),
                  RED, image_tag.ljust(col_width1), CYAN, image_id, NC)

            image_to_remove = ''
            if action == 'dangling':
                image_to_remove = image_id
            else:
                # Errors can occur trying to delete an image tagged
                # with <none> that is not dangling.
                if image_tag != '<none>':
                    image_to_remove = image_name + ':' + image_tag

            if image_to_remove != '':
                dockerutils.docker_image_rm(image_to_remove)
                i_did_something = True

        print(BLUE, '*'.center(80, '*'), NC)

    # If no images exist, print a nice message
    else:
        if action == 'all':
            print_action = ''
        else:
            print_action = action

        print(GREEN, '*'.center(80, '*'), NC)
        print(GREEN, '**', GREEN, 'No', print_action, 'images for this guy:',
              NC, CYAN, nb_image_setup.name, NC)
        print(GREEN, '*'.center(80, '*'), NC)

    return i_did_something


def setup_and_delete_images(image_name, nb_image_setup, action):

    # Check if nb_image_setup returns something valid.
    if nb_image_setup.valid:

        # List dangling images and delete them
        images_removed = docker_list_and_delete_images(nb_image_setup, action)

        # List images again, should give a different message this time
        if images_removed:
            docker_list_and_delete_images(nb_image_setup, action)

        # When removing dangling images, sometimes dangling verisons of the
        # base iamge can be left behind, so remove those too:
        if action == 'dangling':
            if nb_image_setup.image_from != 'undefined':
                image_base_name = nb_image_setup.image_from
                nb_image_base_setup = cni.notebook_setup(image_base_name)
                setup_and_delete_images(image_base_name, nb_image_base_setup,
                                        action)

    else:

        # Print an error message
        print(RED, '*'.center(80, '*'), NC)
        print(RED, '**', BLUE, 'The notebook name ', CYAN, image_name, NC,
              BLUE, 'is not valid.', NC)
        print(RED, '*'.center(80, '*'), NC)


########
# MAIN #
########

def main(argv):
    '''
    The main function - usage and help, argument parsing
    '''

    # Setup options
    parser = argparse.ArgumentParser(
        description='Reduce (downsample) an image contained in an hdf5 file')
    parser.add_argument("input_string", nargs='*',
                        help='The input hdf5 file name')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='debug output')
    parser.add_argument('--dangling', action='store_true',
                        help='Remove dangling images associated with this '
                             'notebook and its base notebook')
    parser.add_argument('--old', action='store_true',
                        help='Remove all images for specified notebook '
                             'that do not have the most recent tag')
    parser.add_argument('--all', action='store_true',
                        help='Remove all images for specified notebook')

    # Print a little extra in addition to the standard help message
    if len(argv) == 0 or '-h' in argv or '--help' in argv:
        try:
            args = parser.parse_args(['-h'])
        except SystemExit:
            print('')
            print('Examples of usage:')
            print(' python3 helpers/remove_images.py --dangling '
                  'test-notebook-ubuntu')
            sys.exit()
    else:
        args = parser.parse_args(argv)

    if args.debug:
        print(args)

    # Parse the image names - could be a long string of names, or a list
    image_names = []
    for element in args.input_string:
        image_names.extend(args.input_string[0].split())

    # Probably only doing one of these actions at a time
    actions = []
    if args.old:
        actions.append('old')
    if args.all:
        actions.append('all')
    if args.dangling:
        actions.append('dangling')

    # Exit if no known action was requested
    if len(actions) == 0:
        print('')
        print(RED, '*'.center(80, '*'), NC)
        print(RED, '**', BLUE,
              'No image type specified.  Feel sad and try again. ', NC)
        print(RED, '*'.center(80, '*'), NC)
        print('')
        sys.exit()

    print('')

    # Loop over each action
    for action in actions:
        # Loop over each specified image
        for image_name in image_names:

            image_name = image_name.strip()
            if args.debug:
                print('image_name:', image_name)

            # Get information about this notebook, taken from
            # notebooks-config.yml and the git commit history
            nb_image_setup = cni.notebook_setup(image_name)

            setup_and_delete_images(image_name, nb_image_setup, action)

    print('')

    llni.list_multiple_images(image_names)


#######################
# RUN THE APPLICATION #
#######################

if __name__ == '__main__':
    main(sys.argv[1:])
