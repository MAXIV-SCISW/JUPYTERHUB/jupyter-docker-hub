"""
A python version of the build script.
Kind of a test to see if I like using python for this instead of bash - still
not sure...
"""

import datetime
import sys
from helpers import configure_notebook_images as cni
from helpers import list_local_notebook_images as llni
from helpers import dockerutils

# Make it purrty
LIGHTPURPLE = "\033[1;35m"
GREEN = "\033[0;32m"
BLUE = "\033[34m"
CYAN = "\033[1;36m"
RED = "\033[31m"
NC = "\033[0m"


def docker_build_arguments_setup(nb_image_setup):

    datestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')

    # Assemble all docker build arguments in a list
    # '--build-arg', 'TAG_FROM=' + str(nb_image_setup.tag_from),
    args = [
        'docker', 'build',
        '--build-arg', 'IMG_FROM=' + str(nb_image_setup.full_reg_image_from),
        '--build-arg', 'TAG=' + str(nb_image_setup.tag),
        '--build-arg', 'NB_DIR=' + str(nb_image_setup.dir),
        '--build-arg', 'NB_IMG_NAME=' + str(nb_image_setup.name),
        '--build-arg', 'BDATE=' + datestamp,
        '--build-arg', 'EDATE=' + str(nb_image_setup.last_commit_date),
        '--build-arg', 'EHASH=' + str(nb_image_setup.last_commit_hash),
        '-t', nb_image_setup.proj + '/' + nb_image_setup.name + ':' +
        nb_image_setup.tag,
        '-f', nb_image_setup.path + '/Dockerfile',
        '--no-cache', '.'
    ]

    # Print what's going to happen to the terminal
    print('')
    print(GREEN, '*'.center(102, '*'), NC)
    print(GREEN, '**', BLUE, ' Building    ', LIGHTPURPLE,
          nb_image_setup.display_name, NC)
    print(GREEN, '**', BLUE, ' image name: ', LIGHTPURPLE, nb_image_setup.proj
          + '/' + nb_image_setup.name, NC)
    print(GREEN, '**', BLUE, ' tag:        ', LIGHTPURPLE, nb_image_setup.tag,
          NC)
    print(GREEN, '*'.center(102, '*'), NC)

    return args


########
# MAIN #
########

def main(argv):

    # Input checks here (should add more)
    if len(argv) == 0:
        print(RED, '**', BLUE, "Bad input, try again!", RED, '**', NC)
        return

    # Some simple testing
    if argv[0] == '-t':
        print(RED, '**', BLUE, "Testing!", RED, '**', NC)
        print(RED, '**', CYAN, 'sys.path:', NC, sys.path)
        return

    # This is dumb - should check if this is actually string, as is assumed
    # here, or if it is an array.
    image_names = argv[0].split()

    for image_name in image_names:
        image_name = image_name.strip()

        # Get information about this image, taken from notebooks-config.yml and
        # the git commit history
        nb_image_setup = cni.notebook_setup(image_name)

        # Check if nb_image_setup returns something valid.
        if nb_image_setup.valid:

            # Setup the arguments for the docker build command
            args = docker_build_arguments_setup(nb_image_setup)

            # Execute the build command
            dockerutils.execute_command_print_output(args)

            # Print information about this image on the local machine
            llni.docker_list_local_image(nb_image_setup)

        else:

            # Print an error message
            print(RED, '*'.center(102, '*'), NC)
            print(RED, '**', BLUE, 'The notebook name ', CYAN, image_name, NC,
                  BLUE, 'is not valid.', NC)
            print(RED, '*'.center(102, '*'), NC)


#######################
# RUN THE APPLICATION #
#######################

if __name__ == '__main__':
    main(sys.argv[1:])
