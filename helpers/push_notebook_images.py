"""
This script looks for images with the most up to date and proper tag,
then makes two tags of this image:
    - both prefixed with the registry address
    - one suffixed with the current proper tag
    - the other suffixed with the tag "stable"
    - any existing image & tag combination will be overwritten
Both images are then pushed to the registry.
"""

#####################
# IMPORT LIBRARIES ##
#####################

# General systems
import sys

from helpers import dockerutils
from helpers import configure_notebook_images as cni
from helpers import list_registry_notebook_images as lrni

# Make it purrty
LIGHTPURPLE = "\033[1;35m"
GREEN = "\033[0;32m"
BLUE = "\033[34m"
CYAN = "\033[1;36m"
RED = "\033[31m"
NC = "\033[0m"


###########
# HELPERS #
###########

def docker_push_images(nb_image_setup):

    # This functions will push two images:
    #   - both prefixed with the registry address
    #       - one suffixed with the current proper tag
    #       - the other with the tag "stable"

    # This returns something if the images exist locally.
    current_tag_exists = dockerutils.does_image_exist_locally(
        nb_image_setup.reg_proj_name_tag)
    stable_tag_exists = dockerutils.does_image_exist_locally(
        nb_image_setup.reg_proj_name_stable)

    # If the images exists, push them
    if current_tag_exists and stable_tag_exists:
        print('')
        print(GREEN, '*'.center(80, '*'), NC)
        print(GREEN, '**', BLUE, 'The images to be pushed exist:', NC)
        print(GREEN, '**  ', CYAN, nb_image_setup.reg_proj_name_tag, NC)
        print(GREEN, '**  ', CYAN, nb_image_setup.reg_proj_name_stable, NC)
        print(GREEN, '**', NC)

        # Print a bit to the terminal about what will happen
        print(GREEN, '**', BLUE, 'Will now push these images to the registry',
              NC, CYAN, nb_image_setup.reg, NC)

        print(GREEN, '*'.center(80, '*'), NC)
        print('')

        # Perform the pushing
        dockerutils.docker_push_image(nb_image_setup.reg_proj_name_tag)
        dockerutils.docker_push_image(nb_image_setup.reg_proj_name_stable)

    # If the images do not exist, complain
    else:
        print('')
        print(RED, '*'.center(80, '*'), NC)
        print(RED, '**', BLUE, 'Missing one or more tagged images:', NC)
        if not current_tag_exists:
            print(RED, '**  ', CYAN, nb_image_setup.reg_proj_name_tag, NC)
        if not stable_tag_exists:
            print(RED, '**  ', CYAN, nb_image_setup.reg_proj_name_stable, NC)
        print(RED, '*'.center(80, '*'), NC)
        print('')


########
# MAIN #
########

def main(argv):

    # print(argv)

    image_name = False

    # Input checks here (should add more)
    if len(argv) > 0:
        image_name = argv[0]
    else:
        print("Bad input, try again!")
        return

    # Get information about this image, taken from notebooks-config.yml and the
    # git commit history
    nb_image_setup = cni.notebook_setup(image_name)

    # Check if nb_image_setup returns something valid.
    if nb_image_setup.valid:

        # Push the two images to the registry
        docker_push_images(nb_image_setup)

        # Print what's in the registry for this image now
        lrni.docker_list_registry_image(nb_image_setup)

    else:
        print('The notebook name ', image_name, 'is not valid.')


#######################
# RUN THE APPLICATION #
#######################

if __name__ == '__main__':
    main(sys.argv[1:])
