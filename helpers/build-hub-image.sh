#!/bin/bash
#
# This is a simple helper script for building the hub image 

# Make it purrty
LIGHTPURPLE="\033[1;35m"
GREEN="\033[32m"
BLUE="\033[34m"
RED="\033[31m"
NC="\033[0m"

# Build the hub
echo ""
echo -e "${BLUE}**********************************************${NC}"
echo -e "${BLUE}** ${LIGHTPURPLE}Building maxiv/jupyterhub:latest"
echo -e "${BLUE}**********************************************${NC}"
docker build -t maxiv/jupyterhub:latest --no-cache .         
echo ""                                                                            
                                                                                   
# Prune!
echo ""
echo -e "${BLUE}**********************************************${NC}"
echo -e "${BLUE}** ${LIGHTPURPLE}Pruning unused data from docker"
echo -e "${BLUE}**********************************************${NC}"
docker system prune -f 
echo ""

# DONE!                                                                            
echo ""                                                                            
echo -e "${BLUE}**********************************************${NC}"               
echo -e "${BLUE}**                   ${GREEN}DONE                   ${BLUE}**"
echo -e "${BLUE}**********************************************${NC}"               
echo ""                                                                            
                                                                                   
exit;    
